/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-12 17:59:18
 * @LastEditors: cxguo
 * @LastEditTime: 2020-05-31 16:30:56
 */
import oftenuse from './oftenuse'
import tableIndex from './table-index'
export { oftenuse, tableIndex }
