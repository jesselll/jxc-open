/*
 * @Descripttion: 财务往来账
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-02 15:03:05
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-05 19:51:57
 */
import axios from '@/utils/request'
const baseUrl = '/log/finAmount'

export function listData(params) {
  return axios.request({
    url: `${baseUrl}/listPage`,
    method: 'post',
    data: params
  })
}
