/*
 * @Descripttion: 商品计量单位接口api
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-02 15:03:05
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-06 13:48:25
 */
import axios from '@/utils/request'
const baseUrl = '/unit'

export function save(params) {
  return axios.request({
    url: `${baseUrl}/save`,
    method: 'post',
    data: params
  })
}

export function delData(params) {
  return axios.request({
    url: `${baseUrl}/del`,
    method: 'post',
    data: params
  })
}

export function listPage(params) {
  return axios.request({
    url: `${baseUrl}/listPage`,
    method: 'post',
    data: params
  })
}

export function listOptions(params) {
  return axios.request({
    url: `${baseUrl}/listOptions`,
    method: 'post',
    data: params
  })
}
