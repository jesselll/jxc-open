/*
 * @Descripttion: 对外的接口，获取价格等的默认值
 * @version:
 * @Author: cxguo
 * @Date: 2020-04-05 08:17:32
 * @LastEditors: cxguo
 * @LastEditTime: 2020-06-09 13:21:59
 */

import { getPriceData, initUpdatePriceData } from './price'
import { initUpdateBarcodeData } from './barcode'
import { initDefaultStockData } from './stock'
import { initDefaultStockWarnData } from './stock-warn'

/**
 * 默认价格
 */
export const getDefaultPrice = function(unitData, goodspriceList, priceType) {
  const originPriceData = initUpdatePriceData(priceType, unitData, goodspriceList)
  return getPriceData(unitData, originPriceData, priceType)
}

/**
 * 默认条形码
 */
export const getDefaultBarcode = function(unitData, goodsBarcodeList) {
  return initUpdateBarcodeData(unitData, goodsBarcodeList)
}

/**
 * 默认库存信息
 */
export const getDefaultStock = function(storeData) {
  return initDefaultStockData(storeData)
}

/**
 * 默认库存预警信息
 */
export const getDefaultStockWarn = function(storeData) {
  return initDefaultStockWarnData(storeData)
}
