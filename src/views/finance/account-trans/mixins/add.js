
/*
 * @Descripttion: 客户的跳转
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-16 09:17:57
 * @LastEditors: cxguo
 * @LastEditTime: 2020-07-09 08:33:42
 */
import moment from 'moment'
import clonedeep from 'clonedeep'

export default {
  data() {
    return {}
  },
  methods: {
    initSaveData() {
      const date = moment().format('YYYY-MM-DD HH:mm:ss')
      this.$set(this.dataObj, 'transTimeIn', date)
      this.$set(this.dataObj, 'transTimeOut', date)
      this.$set(this.dataObj, 'procedureUser', 0)
    },
    initUpdateData() {
      this.dataObj = clonedeep(this.data)
    }
  }
}

