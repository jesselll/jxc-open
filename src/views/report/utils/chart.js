/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2020-05-30 09:28:39
 * @LastEditors: cxguo
 * @LastEditTime: 2020-05-30 12:22:29
 */
import moment from 'moment'

/**
 * 获取当前时间间隔是 日(diff < 30)，月(365 >= diff >= 30)，年(diff > 365)
 * 图表x轴，日期类型（年or月or日）
 */
export const getChartTimeType = function(beginTime, endTime) {
  if (!beginTime || !endTime) return 0
  const b = moment(beginTime)
  const e = moment(endTime)
  const diff = Math.abs(e.diff(b, 'days'))
  if (diff <= 30) return 1
  if (diff >= 365) return 3
  return 2
}

/**
 * 获取按日时候的chart数据
 */
const getChartData = function(data, timesArr, timeSuffix) {
  const xData = []
  const yData = []
  timesArr.forEach(i => {
    xData.push(`${i}${timeSuffix}`)
    const times = data.filter(item => { return Number(item.time) === i })
    if (times.length > 0) {
      const time = times[0]
      yData.push(time.amountPayable)
    } else {
      yData.push(0)
    }
  })
  return { xData, yData }
}

export const getChartDayData = function(data, beginTime, endTime) {
  const b = moment(beginTime)
  const e = moment(endTime)
  const diff = Math.abs(e.diff(b, 'days'))
  const beginDay = b.format('D')
  const timesArr = []
  for (let i = 0; i <= diff; i++) {
    timesArr.push(Number(beginDay) + i)
  }
  const timeSuffix = '日'
  return getChartData(data, timesArr, timeSuffix)
}

export const getChartMonthData = function(data, beginTime, endTime) {
  const b = moment(beginTime)
  const e = moment(endTime)
  const diff = Math.abs(e.diff(b, 'months'))
  const beginFormat = b.format('M')
  const timesArr = []
  for (let i = 0; i <= diff; i++) {
    timesArr.push(Number(beginFormat) + i)
  }
  const timeSuffix = '月'
  return getChartData(data, timesArr, timeSuffix)
}

export const getChartYearData = function(data, beginTime, endTime) {
  const b = moment(beginTime)
  const e = moment(endTime)
  const diff = Math.abs(e.diff(b, 'years'))
  const beginFormat = b.format('YYYY')
  const timesArr = []
  for (let i = 0; i <= diff; i++) {
    timesArr.push(Number(beginFormat) + i)
  }
  const timeSuffix = '年'
  return getChartData(data, timesArr, timeSuffix)
}
