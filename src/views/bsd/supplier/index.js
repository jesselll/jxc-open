/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-20 15:54:29
 * @LastEditors: cxguo
 * @LastEditTime: 2020-03-07 16:35:37
 */
import list from './list'
import detail from './detail'
export { list, detail }
export default list
