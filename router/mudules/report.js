/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2019-12-13 17:04:37
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-12 15:50:34
 */
import Layout from '@/layout'

export default {
  path: '/report',
  component: Layout,
  redirect: 'noRedirect',
  meta: { title: '报表', icon: 'baobiao' },
  name: 'Report',
  children: [
    {
      path: 'report_sale',
      name: 'ReportSale',
      component: () => import('@/views/report/list-sale'),
      meta: { title: '销售报表', pid: 'report1', perm: 'report_sale:list' }
    },
    {
      path: 'report_sale_detail_by_good',
      name: 'ReportSaleDetailByGood',
      hidden: true,
      props: (route) => ({
        skuId: route.query.skuId,
        skuName: route.query.skuName,
        beginTime: route.query.beginTime,
        endTime: route.query.endTime,
        tab: route.query.tab
      }),
      component: () => import('@/views/report/list-sale/detail-by-good'),
      meta: { title: '销售明细按商品', pid: 'report1', perm: 'report_sale:list' }
    },
    {
      path: 'report_sale_detail_by_comego',
      name: 'ReportSaleDetailByComego',
      hidden: true,
      props: (route) => ({
        comegoId: route.query.comegoId,
        comegoName: route.query.comegoName,
        beginTime: route.query.beginTime,
        endTime: route.query.endTime,
        tab: route.query.tab
      }),
      component: () => import('@/views/report/list-sale/detail-by-comego'),
      meta: { title: '销售明细按客户', pid: 'report1', perm: 'report_sale:list' }
    },
    {
      path: 'report_sale_detail_by_bill',
      name: 'ReportSaleDetailByBill',
      hidden: true,
      props: (route) => ({
        time: route.query.time
      }),
      component: () => import('@/views/report/list-sale/detail-by-bill'),
      meta: { title: '销售明细按单据', pid: 'report1', perm: 'report_sale:list' }
    },
    {
      path: 'report_sale_detail',
      name: 'ReportSaleDetail',
      component: () => import('@/views/report/list-sale-detail'),
      meta: { title: '销售明细表', pid: 'report1', perm: 'report_sale:list' }
    },
    {
      path: 'report_purchase',
      component: () => import('@/views/report/list-purchase'),
      name: 'ReportPurchase',
      meta: { title: '进货报表', pid: 'report1', perm: 'report_purchase:list' }
    },
    {
      path: 'report_purchase_detail_by_good',
      name: 'ReportPurchaseDetailByGood',
      hidden: true,
      props: (route) => ({
        skuId: route.query.skuId,
        skuName: route.query.skuName,
        beginTime: route.query.beginTime,
        endTime: route.query.endTime,
        tab: route.query.tab
      }),
      component: () => import('@/views/report/list-purchase/detail-by-good'),
      meta: { title: '进货明细按商品', pid: 'report1', perm: 'report_purchase:list' }
    },
    {
      path: 'report_purchase_detail_by_comego',
      name: 'ReportPurchaseDetailByComego',
      hidden: true,
      props: (route) => ({
        comegoId: route.query.comegoId,
        comegoName: route.query.comegoName,
        beginTime: route.query.beginTime,
        endTime: route.query.endTime,
        tab: route.query.tab
      }),
      component: () => import('@/views/report/list-purchase/detail-by-comego'),
      meta: { title: '进货明细按供应商', pid: 'report1', perm: 'report_purchase:list' }
    },
    {
      path: 'report_purchase_detail_by_bill',
      name: 'ReportPurchaseDetailByBill',
      hidden: true,
      props: (route) => ({
        time: route.query.time
      }),
      component: () => import('@/views/report/list-purchase/detail-by-bill'),
      meta: { title: '进货明细按单据', pid: 'report1', perm: 'report_purchase:list' }
    }
  ]
}
