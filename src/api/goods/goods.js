/*
 * @Descripttion: 商品接口
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-02 15:03:05
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-10 14:08:44
 */
import axios from '@/utils/request'
import { getDomain } from '@/utils/system.js'
const baseUrl = '/good'

export const uploadUrl = getDomain() + `${baseUrl}/import`

export function addData(params) {
  return axios.request({
    url: `${baseUrl}/addAssemblyData`,
    method: 'post',
    data: params
  })
}

export function getTemplate(params) {
  return axios.request({
    url: `${baseUrl}/getTemplate`,
    method: 'post',
    data: params,
    responseType: 'blob'
  })
}

export function exportData(params) {
  return axios.request({
    url: `${baseUrl}/export`,
    method: 'post',
    data: params,
    responseType: 'blob'
  })
}
