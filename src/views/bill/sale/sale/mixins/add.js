
/*
 * @Descripttion: 客户的跳转
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-16 09:17:57
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-11 20:02:41
 */
import moment from 'moment'
import { getBillcode } from '@/api/bill/bill-sale.js'
export default {
  data() {
    return {}
  },
  methods: {
    initSaveData() {
      const date = moment().format('YYYY-MM-DD HH:mm:ss')
      const defaultPrice = this.$amount(0).format()
      this.dataObj = {
        discountType: '0',
        amountPayable: defaultPrice,
        amountPaid: defaultPrice,
        amountOther: defaultPrice,
        businessTime: date,
        discountValue: '100.00',
        handUserId: this.currentUserId
      }
      this.billExtSale = { type: '1', deliveryDate: date }
      this.getBillCode()
      this.initOptions()
    },
    initOptions() {
      this.getSaleType()
    },
    getBillCode() {
      getBillcode().then(res => {
        if (!res.data.flag) this.$message.error('获取票据编号失败！')
        const code = res.data.data
        this.$set(this.dataObj, 'billNo', code)
      })
    }
  }
}

