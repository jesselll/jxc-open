/*
 * @Descripttion: 商品分类接口
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-02 15:03:05
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-05 09:42:10
 */
import axios from '@/utils/request'
const baseUrl = '/customerCat'

export function listData(params) {
  return axios.request({
    url: `${baseUrl}/listAllData`,
    method: 'post',
    data: params
  })
}

export function saveData(params) {
  return axios.request({
    url: `${baseUrl}/saveData`,
    method: 'post',
    data: params
  })
}

export function delData(params) {
  return axios.request({
    url: `${baseUrl}/delete`,
    method: 'post',
    data: params
  })
}
