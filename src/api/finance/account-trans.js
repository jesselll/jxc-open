/*
 * @Descripttion: 财务往来账
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-02 15:03:05
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-05 15:33:25
 */
import axios from '@/utils/request'
const baseUrl = '/fin/accountTrans'

export function addData(params) {
  return axios.request({
    url: `${baseUrl}/addData`,
    method: 'post',
    data: params
  })
}
/**
 * 作废bill
 * @param {*} params
 */
export function cancleBill(params) {
  return axios.request({
    url: `${baseUrl}/cancle`,
    method: 'post',
    data: params
  })
}

export function listData(params) {
  return axios.request({
    url: `${baseUrl}/listPage`,
    method: 'post',
    data: params
  })
}

export function getDataById(id) {
  return axios.request({
    url: `${baseUrl}/getData/${id}`,
    method: 'post',
    data: id
  })
}

