/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2020-05-31 16:38:27
 * @LastEditors: cxguo
 * @LastEditTime: 2020-06-15 12:05:51
 */

const routerMaps = [
  { code: 'sale1', name: '销售' },
  { code: 'stock1', name: '进货' },
  { code: 'stock2', name: '库存' },
  { code: 'stock3', name: '查询' },
  { code: 'finance1', name: '往来账' },
  { code: 'finance2', name: '更多账务' },
  { code: 'finance3', name: '查询' },
  { code: 'report1', name: '进销存' },
  { code: 'report2', name: '财务' },
  { code: 'finance3', name: '查询' },
  { code: 'basedata1', name: '商品' },
  { code: 'basedata2', name: '往来单位' },
  { code: 'setup1', name: '店铺设置' },
  { code: 'setup2', name: '参数设置' },
  { code: 'setup3', name: '账户信息' }
]

export const getNameByCode = function(code) {
  const routerMapArr = routerMaps.filter(item => { return item.code === code })
  if (routerMapArr.length < 1) {
    console.error(`找不到[${code}]的router`)
    return
  }
  return routerMapArr[0].name
}
