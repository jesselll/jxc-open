/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2020-04-16 22:20:19
 * @LastEditors: cxguo
 * @LastEditTime: 2020-08-06 15:07:19
 */
import GoodsSelect from '@/views/goods-select'

export default {
  extends: GoodsSelect,
  mixins: [GoodsSelect],
  props: {
  },
  data() {
    return {
      validRules: {
        quantity: [
          { required: true, message: '商品数量必填' },
          { trigger: 'change', validator: ({ row, cellValue }) => {
            const { maxq } = row
            return new Promise((resolve, reject) => {
              if (Number(cellValue) > Number(maxq)) {
                reject(new Error('"出库数量"不能超过"未出库数量"！'))
              } else {
                resolve()
              }
            })
          }
          }
        ]
      }
    }
  },
  created() {
    const originColumnsData = this.originColumnsData
    const nameCol = originColumnsData.find(item => { return item.field === 'name' })
    const unitCol = originColumnsData.find(item => { return item.field === 'unitName' })
    this.$delete(nameCol, 'editRender')
    this.$delete(unitCol, 'editRender')
    for (let i = 0; i < originColumnsData.length; i++) {
      const item = originColumnsData[i]
      if (['price', 'total'].indexOf(item.field) !== -1) {
        originColumnsData.splice(i, 1)
        i--
      }
    }
    this.originColumnsData = originColumnsData
  },
  methods: {
  }
}
