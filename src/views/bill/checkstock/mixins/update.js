
/*
 * @Descripttion: 客户的跳转
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-16 09:17:57
 * @LastEditors: cxguo
 * @LastEditTime: 2020-06-26 15:59:59
 */
import { getBillDetailById } from '@/api/bill/bill-inventory.js'
import moment from 'moment'

export default {
  data() {
    return {}
  },
  methods: {
    initViewData() {
      const billId = this.billId
      return this.getBillContainGoodsData(billId).then(data => {
        const { bill, billDetailExtInventoryList } = data
        Object.keys(bill).forEach(key => {
          this.$set(this.dataObj, key, bill[key])
        })
        this.setTableData('table', billDetailExtInventoryList)
      })
    },
    initCopyData() {
      const billId = this.billId
      return this.getBillContainGoodsData(billId).then(data => {
        const { bill, billDetailExtInventoryList } = data
        Object.keys(bill).forEach(key => {
          this.$set(this.dataObj, key, bill[key])
        })
        this.$refs.GoodsSelect.setTableData(billDetailExtInventoryList)
        //
        this.getBillCode()
        const date = moment().format('YYYY-MM-DD HH:mm:ss')
        this.$set(this.dataObj, 'businessTime', date)
        this.$set(this.dataObj, 'id', null)
      })
    },
    getBillContainGoodsData(billId) {
      return new Promise((resolve, reject) => {
        this.setTableLoading('table', true)
        getBillDetailById(billId).then(res => {
          this.setTableLoading('table', false)
          if (!res.data.flag) {
            return reject(new Error('获取采购单商品失败！'))
          }
          const data = res.data.data
          resolve(data)
        }).catch(err => {
          reject(err)
        })
      })
    }
  }
}

