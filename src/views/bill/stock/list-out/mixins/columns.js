/*
 * @Descripttion: 客户的跳转
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-16 09:17:57
 * @LastEditors: cxguo
 * @LastEditTime: 2020-07-06 10:15:38
 */

import { getMoreButton, getTransButton, getDelButton } from '@/components/Table/oper-btns.js'
import clonedeep from 'clonedeep'

export default {
  data() {
    return {
      columnsHasInData: [
        { title: '操作', width: 80,
          slots: {
            default: (params) => {
              const { row } = params
              const h = this.$createElement
              const viewMoreNode = getMoreButton(h, { on: { click: () => { this.btnViewData(row) } }})
              const delBtnNode = getDelButton(h, {
                props: { type: 'primary' },
                attrs: { title: '作废' },
                on: { click: () => { this.btnTransData(row) } }
              })
              const divNode = h('div', {
                attrs: {
                  class: 'table-oper-btns'
                }
              }, [viewMoreNode, delBtnNode])
              return [divNode]
            }
          }
        },
        { field: 'billNo', title: '单据编号', width: 150 },
        { field: 'billRelationNo', title: '关联业务编号', width: 150 },
        { field: 'businessTime', title: '业务日期', width: 150 },
        { field: 'comegoName', title: '往来单位', width: 200, showOverflow: true },
        { field: 'goodsNamestr', title: '出库商品', width: 200, showOverflow: true },
        { field: 'goodsQuantity', title: '出库量', width: 100 }
      ],
      columnsWaiteInData: [
        { title: '操作', width: 80,
          slots: {
            default: (params) => {
              const { row } = params
              const h = this.$createElement
              const viewMoreNode = getMoreButton(h, { on: { click: () => { this.btnViewData(row) } }})
              const transBtnNode = getTransButton(h, {
                props: { type: 'primary' },
                attrs: { title: '出货' },
                on: { click: () => { this.btnTransData(row) } }
              })
              const divNode = h('div', {
                attrs: {
                  class: 'table-oper-btns'
                }
              }, [viewMoreNode, transBtnNode])
              return [divNode]
            }
          }
        },
        { field: 'billNo', title: '单据编号', width: 150 },
        { field: 'businessTime', title: '业务日期', width: 150 },
        { field: 'billCatName', title: '类型', width: 100 },
        { field: 'comegoName', title: '往来单位', width: 200, showOverflow: true },
        { field: 'goodsNamestr', title: '出库商品', width: 200, showOverflow: true },
        { field: 'goodsQuantity', title: '待出库量', width: 80,
          slots: {
            default: (params) => {
              const { row } = params
              const { goodsQuantity, changeQuantity } = row
              const value = goodsQuantity - changeQuantity
              return [<span>{value}</span>]
            }
          }}
      ]
    }
  },
  methods: {
  },
  computed: {
    columnsData: function(params) {
      const changeType = this.dataObj.changeType
      if (changeType === 1) return this.originColumnsData
      const cols = clonedeep(this.originColumnsData)
      if (changeType === 2) {
        cols.splice(3, 1)
      }
      if (changeType === 3) {
        const col = { field: 'billNo', title: '进货单编号',
          positionDisable: true,
          slots: {
            default: (params) => {
              // eslint-disable-next-line no-unused-vars
              const h = this.$createElement
              const { row } = params
              const { billNo } = row
              const _this = this
              const data = {
                props: {
                  type: 'primary'
                },
                on: {
                  click: function(e) {
                    const { billId } = row
                    _this.jump2purchaseView({ billId })
                  }
                }
              }
              return [<el-link {...data}>{billNo}</el-link>]
            }
          }
        }
        cols.splice(3, 1, col)
      }
      return cols
    }
  }
}

