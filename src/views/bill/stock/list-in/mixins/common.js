
/*
 * @Descripttion: 客户的跳转
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-16 09:17:57
 * @LastEditors: cxguo
 * @LastEditTime: 2020-06-22 13:57:01
 */
import { getBillcode } from '@/api/bill/bill-stock-in.js'

export default {
  data() {
    return {}
  },
  methods: {
    getBillCode() {
      getBillcode().then(res => {
        if (!res.data.flag) this.$message.error('获取票据编号失败！')
        const code = res.data.data
        this.$set(this.dataObj, 'billNo', code)
      })
    }
  }
}

