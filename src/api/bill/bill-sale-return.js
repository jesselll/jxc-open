/*
 * @Descripttion: 票据接口
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-02 15:03:05
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-04 11:12:33
 */
import axios from '@/utils/request'
const baseUrl = '/bill/saleReturn'

/**
 * 添加销售退货单
 * @param {*} params
 */
export function addSaleReturn(params) {
  return axios.request({
    url: `${baseUrl}/addData`,
    method: 'post',
    data: params
  })
}

/**
 * 删除票据
 * @param {} params
 */
export function delData(params) {
  return axios.request({
    url: `${baseUrl}/delete`,
    method: 'post',
    data: params
  })
}

export function cancleBill(params) {
  return axios.request({
    url: `${baseUrl}/cancle`,
    method: 'post',
    data: params
  })
}

/**
 * 查询销售单列表
 * @param {*} params
 */
export function listSaleReturn(params) {
  return axios.request({
    url: `${baseUrl}/listPage`,
    method: 'post',
    data: params
  })
}

export function listWaite2ReturnBillsPage(params) {
  return axios.request({
    url: `${baseUrl}/listWaite2ReturnBillsPage`,
    method: 'post',
    data: params
  })
}

export function getBillAndDetail(params) {
  return axios.request({
    url: `${baseUrl}/getBillAndDetails/${params}`,
    method: 'post',
    data: params
  })
}

/**
 * 生成票据编号
 * @param {*} params
 */
export function getBillcode(params) {
  return axios.request({
    url: `${baseUrl}/getBillNo`,
    method: 'post',
    data: params
  })
}

export function print(params) {
  return axios.request({
    url: `${baseUrl}/print`,
    method: 'post',
    data: params
  })
}
