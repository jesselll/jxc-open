/** 权限指令 */
import hasPerm from './hasPerm'
import hasRole from './hasRole'

export const perm = {
  bind: function(el, binding) {
    if (el && binding.value) {
      // console.log('current user has required permission: ' + binding.value + '? --> ' + hasPerm(binding.value))
      if (!hasPerm(binding.value)) {
        // 移除 el 元素
        el.parentNode.removeChild(el)
      }
    }
  }
}

export const role = {
  componentUpdated: function(el, binding, vnode) {
    const role = binding.value
    if (el && binding.value) {
      if (!hasRole(role)) {
        el.parentNode && el.parentNode.removeChild(el)
      }
    }
  }
}
