import draggable from './module/draggable'
import clipboard from './module/clipboard'
import { perm, role } from './module/permission'
import enterfocus from './module/enterfocus'

const directives = {
  draggable,
  clipboard,
  perm,
  role,
  enterfocus
}
export default directives
