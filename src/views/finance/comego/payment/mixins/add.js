
/*
 * @Descripttion: 客户的跳转
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-16 09:17:57
 * @LastEditors: cxguo
 * @LastEditTime: 2020-06-21 17:57:42
 */
import moment from 'moment'
import { genBillcode } from '@/api/bill/bill.js'
export default {
  data() {
    return {}
  },
  methods: {
    initSaveData() {
      this.$set(this.dataObj, 'borrowType', '0')
      this.getBillCode('borrowin')
      const date = moment().format('YYYY-MM-DD HH:mm:ss')
      this.$set(this.dataObj, 'businessTime', date)
      this.$set(this.dataObj, 'amountOther', '0.00')
    },
    getBillCode(type) {
      genBillcode(type).then(res => {
        if (!res.data.flag) this.$message.error('获取票据编号失败！')
        const code = res.data.data
        this.$set(this.dataObj, 'billNo', code)
      })
    }
  }
}

