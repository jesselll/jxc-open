/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2019-11-06 21:44:06
 * @LastEditors: cxguo
 * @LastEditTime: 2020-07-28 09:55:36
 */
import Barcode from './barcode'
import Base from './base'
import BaseSku from './base-sku'
import Price from './price'
import Unit from './unit'
import Specs from './specs'
import SpecsSku from './specs-sku'
import Stock from './stock'
import StockWarn from './stock-warn'
export {
  Barcode,
  Base,
  BaseSku, SpecsSku, Price, Unit, Specs, Stock, StockWarn }
